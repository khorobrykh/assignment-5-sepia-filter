#ifndef LAB5_SEPIA_ASM_H
#define LAB5_SEPIA_ASM_H
#include "imageUtils.h"

void sepia_asm(struct image *img);

#endif //LAB5_SEPIA_ASM_H
