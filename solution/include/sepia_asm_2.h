#ifndef LAB5_SEPIA_ASM_2_H
#define LAB5_SEPIA_ASM_2_H
#include "imageUtils.h"

void sepia_asm_2(struct image *img);

#endif //LAB5_SEPIA_ASM_2_H
