#ifndef LAB5_SEPIA_H
#define LAB5_SEPIA_H

#include "imageUtils.h"
#include "stdlib.h"

void sepia(const struct image *source, struct image *dest);

#endif //LAB5_SEPIA_H
