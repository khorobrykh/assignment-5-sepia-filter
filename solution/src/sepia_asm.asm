global sepia_asm

section .data
red0: db 0.393
red1: db 0.769
red2: db 0.189
green0: db 0.349
green1: db 0.686
green2: db 0.168
blue0: db 0.272
blue1: db 0.534
blue2: db 0.131

section .text
sepia_asm:
    mov rax, [rdi] ; rax - ширина изображения
    mov rbx, [rdi + 8] ; rbx - высота изображения
    mov rcx, [rdi + 16] ; rcx - указатель data

    xor rdx, rdx ; внешний счетчик по height

    .outer_loop:
        cmp rdx, rbx
        je .end
        xor rsi, rsi ; внутренний счетчик по width

        .inner_loop:
            cmp rsi, rax
            je .end_inner

            mov r8, rdx
            imul r8, rax
            add r8, rsi
            imul r8, 3

            movzx r9, byte [rcx + r8] ; blue
            movzx r10, byte [rcx + r8 + 1] ; green
            movzx r11, byte [rcx + r8 + 2] ; red

            call sepia_transform

            mov byte [rcx + r8], r9b
            mov byte [rcx + r8 + 1], r10b
            mov byte [rcx + r8 + 2], r11b

            inc rsi
            jmp .inner_loop

        .end_inner:
            inc rdx
            jmp .outer_loop

    .end:
        ret

sepia_transform:
    movzx r15, byte [red0]
    movzx r14, byte [red1]
    movzx r13, byte [red2]

    imul r15, r11
    imul r14, r10
    imul r13, r9

    add r15, r14
    add r15, r13

    shr r15, 8

    mov r11, r15

    movzx r15, byte [green0]
    movzx r14, byte [green1]
    movzx r13, byte [green2]

    imul r15, r11
    imul r14, r10
    imul r13, r9

    add r15, r14
    add r15, r13

    shr r15, 8

    mov r10, r15

    movzx r15, byte [blue0]
    movzx r14, byte [blue1]
    movzx r13, byte [blue2]

    imul r15, r11
    imul r14, r10
    imul r13, r9

    add r15, r14
    add r15, r13

    shr r15, 8

    mov r9, r15

    ret
