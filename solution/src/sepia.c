#include "../include/sepia.h"

void sepia(const struct image *source, struct image *dest) {
    for (size_t i = 0; i < source->height; i++) {
        for (size_t j = 0; j < source->width; j++) {
            uint8_t red = source->data[i * source->width + j].r;
            uint8_t green = source->data[i * source->width + j].g;
            uint8_t blue = source->data[i * source->width + j].b;

            int new_red = (int) (0.393 * red + 0.769 * green + 0.189 * blue);
            int new_green = (int) (0.349 * red + 0.686 * green + 0.168 * blue);
            int new_blue = (int) (0.272 * red + 0.534 * green + 0.131 * blue);

            dest->data[i * source->width + j].r = (uint8_t) (new_red < 256 ? new_red : 255);
            dest->data[i * source->width + j].g = (uint8_t) (new_green < 256 ? new_green : 255);
            dest->data[i * source->width + j].b = (uint8_t) (new_blue < 256 ? new_blue : 255);

        }
    }
}
