#include "bmp.h"
#include "sepia.h"
#include "sepia_asm.h"
#include "sepia_asm_2.h"
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <time.h>

#define WRONG_ARGS_COUNT "Wrong args count. Example: main.c <in.bmp> <out_c.bmp> <out_asm.bmp>"
#define MEMORY_ERROR "Memory error"
#define FILE_OPENING_ERROR "File opening error"
#define ERROR_EXIT_CODE 1
#define EXIT_CODE 0
#define FILE_WRITE_MODE "wb"
#define FILE_READ_MODE "rb"

void make_tests(struct image* in, struct image* outC, struct image* outAsm, int n) {
    clock_t beginC = clock();
    for (int i = 0; i < n; ++i) {
        sepia((const struct image *) in, outC);
    }
    clock_t endC = clock();

    clock_t beginAsm = clock();
    for (int i = 0; i < n; ++i) {
        // Для замера времени работы фильтра здесь заново не копируется исходная картинка в итоговую,
        // из-за чего в результате получается черная картинка вследствие применения сепии 100 раз подряд
        sepia_asm_2(outAsm);
    }
    clock_t endAsm = clock();

    double time_spentC = (double) (endC - beginC) / CLOCKS_PER_SEC;
    double time_spentAsm = (double) (endAsm - beginAsm) / CLOCKS_PER_SEC;

    printf("%d times\n", n);
    printf("C time is %f seconds\n", time_spentC);
    printf("ASM time is %f seconds\n", time_spentAsm);
}

int main(int argc, char **argv) {
    if (argc != 4) {
        perror(WRONG_ARGS_COUNT);
        return ERROR_EXIT_CODE;
    }

    FILE *fileIn = fopen(argv[1], FILE_READ_MODE);
    if (fileIn == NULL) {
        perror(FILE_OPENING_ERROR);
        return ERROR_EXIT_CODE;
    }
    struct image img = {0};

    enum read_status rs = from_bmp(fileIn, &img);
    if (rs != READ_OK) {
        perror(readStatusStringRepresentation(rs));
        return ERROR_EXIT_CODE;
    } else {
        printf("%s\n", readStatusStringRepresentation(rs));
    }

    fclose(fileIn);

    struct image result = {0};
    result.width = img.width;
    result.height = img.height;
    result.data = malloc((sizeof(struct pixel) * img.width * img.height));
    if (result.data == NULL) {
        perror(MEMORY_ERROR);
        return ERROR_EXIT_CODE;
    }

    struct image resultAsm = {0};
    resultAsm.width = img.width;
    resultAsm.height = img.height;
    resultAsm.data = malloc((sizeof(struct pixel) * img.width * img.height));
    if (resultAsm.data == NULL) {
        perror(MEMORY_ERROR);
        return ERROR_EXIT_CODE;
    }
    memcpy(resultAsm.data, img.data, sizeof(struct pixel) * img.width * img.height);

     make_tests(&img, &result, &resultAsm, 1);
     printf("---------------------------------------\n");
     make_tests(&img, &result, &resultAsm, 10);
     printf("---------------------------------------\n");
     make_tests(&img, &result, &resultAsm, 100);

    memcpy(resultAsm.data, img.data, sizeof(struct pixel) * img.width * img.height);
    sepia_asm_2(&resultAsm);

    FILE *fileOut = fopen(argv[2], FILE_WRITE_MODE);
    if (fileOut == NULL) {
        perror(FILE_OPENING_ERROR);
        return ERROR_EXIT_CODE;
    }
    enum write_status ws = to_bmp(fileOut, &result);
    free(img.data);

    if (ws != WRITE_OK) {
        perror(writeStatusStringRepresentation(ws));
    } else {
        printf("%s\n", writeStatusStringRepresentation(ws));
    }

    FILE *fileOutAsm = fopen(argv[3], FILE_WRITE_MODE);
    if (fileOutAsm == NULL) {
        perror(FILE_OPENING_ERROR);
        return ERROR_EXIT_CODE;
    }
    enum write_status wsAsm = to_bmp(fileOutAsm, &resultAsm);

    if (wsAsm != WRITE_OK) {
        perror(writeStatusStringRepresentation(ws));
    } else {
        printf("%s\n", writeStatusStringRepresentation(ws));
    }

    fclose(fileOut);
    free(result.data);
    free(resultAsm.data);
    return EXIT_CODE;
}
