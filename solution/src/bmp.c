#include "bmp.h"

#define BF_TYPE 19778
#define BF_RESERVED 0
#define BF_OFFBITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BITCOUNT 24
#define BI_COMPRESSION 0
#define BI_XPELSPERMETER 0
#define BI_YPELSPERMETER 0
#define BI_CLRUSED 0
#define BI_CLRIMPORTANT 0


enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_ERROR;
    }
    if (header.bfType != BF_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.bOffBits != BF_OFFBITS) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != BI_BITCOUNT) {
        return READ_INVALID_HEADER;
    }
    if (fseek(in, (long) header.bOffBits, SEEK_SET)) {
        return READ_INVALID_HEADER;
    }

    uint32_t width = header.biWidth;
    uint32_t height = header.biHeight;
    long padding = calculatePadding(width);

    if (!initImage(img, width, height)) {
        return READ_MEMORY_ERROR;
    };

    for (size_t i = 0; i < height; i++) {
        if (fread((img->data) + i * width, sizeof(struct pixel), width, in) != width) {
            return READ_INVALID_BITS;
        }
        if (fseek(in, padding, SEEK_CUR)) {
            return READ_ERROR;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img) {
    struct bmp_header header = {0};
    long padding = calculatePadding(img->width);
    uint32_t sizeImage = sizeof(struct pixel) * (img->width + padding) * img->height;
    uint32_t fileSize = sizeof(struct bmp_header) + sizeImage;

    header.bfType = BF_TYPE;
    header.bfileSize = fileSize;
    header.bfReserved = BF_RESERVED;
    header.bOffBits = BF_OFFBITS;
    header.biSize = BI_SIZE;
    header.biWidth = (uint32_t) img->width;
    header.biHeight = (uint32_t) img->height;
    header.biPlanes = BI_PLANES;
    header.biBitCount = BI_BITCOUNT;
    header.biCompression = BI_COMPRESSION;
    header.biSizeImage = sizeImage;
    header.biXPelsPerMeter = BI_XPELSPERMETER;
    header.biYPelsPerMeter = BI_YPELSPERMETER;
    header.biClrUsed = BI_CLRUSED;
    header.biClrImportant = BI_CLRIMPORTANT;

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) !=
            img->width) {
            return WRITE_ERROR;
        }
        if (fseek(out, padding, SEEK_CUR)) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

const char* readStatusStringRepresentation(enum read_status rs) {
    switch (rs) {
        case READ_OK:
            return "Read file ok";
        case READ_INVALID_SIGNATURE:
            return "Read invalid signature";
        case READ_INVALID_BITS:
            return "Read invalid bits";
        case READ_INVALID_HEADER:
            return "Read invalid header";
        case READ_MEMORY_ERROR:
            return "Read memory error";
        case READ_ERROR:
            return "Read error";
        default:
            return "";
    }
}

const char* writeStatusStringRepresentation(enum write_status ws) {
    switch (ws) {
        case WRITE_OK:
            return "Write file ok";
        case WRITE_ERROR:
            return "Write error";
        default:
            return "";
    }
}
