global sepia_asm_2

section .data
align 16
red: dd 0.272, 0.349, 0.393, 0
green: dd 0.534, 0.686, 0.769, 0
blue: dd 0.131, 0.168, 0.189, 0

section .text
sepia_asm_2:
    mov rax, [rdi] ; rax - ширина
    mov rbx, [rdi + 8] ; rbx - высота
    mov rcx, [rdi + 16] ; rcx - data

    pxor xmm0, xmm0

    xor rdx, rdx ; внешний счетчик по высоте
    .outer_loop:
        cmp rdx, rbx
        je .end_outer

        xor rsi, rsi ; внутренний счетчик по ширине

        .inner_loop:
            cmp rsi, rax
            je .end_inner

            mov r8, rdx
            imul r8, rax
            add r8, rsi
            imul r8, 3

            pxor xmm1, xmm1
            pinsrb xmm1, [rcx + r8], 0
            pinsrb xmm1, [rcx + r8], 4
            pinsrb xmm1, [rcx + r8], 8
            cvtdq2ps xmm1, xmm1

            pxor xmm2, xmm2
            pinsrb xmm2, [rcx + r8 + 1], 0
            pinsrb xmm2, [rcx + r8 + 1], 4
            pinsrb xmm2, [rcx + r8 + 1], 8
            cvtdq2ps xmm2, xmm2

            pxor xmm3, xmm3
            pinsrb xmm3, [rcx + r8 + 2], 0
            pinsrb xmm3, [rcx + r8 + 2], 4
            pinsrb xmm3, [rcx + r8 + 2], 8
            cvtdq2ps xmm3, xmm3

            movdqa xmm4, [blue]
            movdqa xmm5, [green]
            movdqa xmm6, [red]

            mulps xmm1, xmm4
            mulps xmm2, xmm5
            mulps xmm3, xmm6

            pxor xmm7, xmm7

            addps xmm7, xmm1
            addps xmm7, xmm2
            addps xmm7, xmm3

            cvtps2dq xmm7, xmm7

            .set1:
            pextrb r10, xmm7, 1
            test r10, r10
            jnz .set255_1
            pextrb [rcx + r8], xmm7, 0
            jmp .set2
            .set255_1:  mov byte [rcx + r8], 0xFF

            .set2:
            pextrb r10, xmm7, 5
            test r10, r10
            jnz .set255_2
            pextrb [rcx + r8 + 1], xmm7, 4
            jmp .set3
            .set255_2:  mov byte [rcx + r8 + 1], 0xFF

            .set3:
            pextrb r10, xmm7, 9
            test r10, r10
            jnz .set255_3
            pextrb [rcx + r8 + 2], xmm7, 8
            jmp .inner_cont
            .set255_3:  mov byte [rcx + r8 + 2], 0xFF

            .inner_cont:
            inc rsi
            jmp .inner_loop

        .end_inner:
            inc rdx
            jmp .outer_loop

    .end_outer:
        ret
