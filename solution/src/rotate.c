#include "imageUtils.h"
#include "rotate.h"
#include <malloc.h>

struct image transpose(struct image const source) {
    struct image dest = {0};
    initImage(&dest, source.height, source.width);
    for (int i = 0; i < dest.height; i++) {
        for (int j = 0; j < dest.width; ++j) {
            dest.data[i * dest.width + j] = source.data[j * source.width + i];
        }
    }
    return dest;
}

struct image reflect(struct image const source) {
    struct image dest = {0};
    initImage(&dest, source.width, source.height);
    for (int i = 0; i < dest.height; i++) {
        for (int j = 0; j < dest.width; j++) {
            dest.data[i * dest.width + j] = source.data[(source.height - i - 1) * source.width + j];
        }
    }
    return dest;
}

struct image rotate(const struct image source, int angle) {
    angle = angle > 0 ? angle : angle + 360;
    int rotations = angle / 90;
    struct image rotatedImage = {0};
    initImage(&rotatedImage, source.width, source.height);
    copyImage(&source, &rotatedImage);
    for (int i = 0; i < rotations; i++) {
        struct image tmp = transpose(rotatedImage);
        free(rotatedImage.data);
        rotatedImage = tmp;

        tmp = reflect(rotatedImage);
        free(rotatedImage.data);
        rotatedImage = tmp;
    }
    return rotatedImage;
}
